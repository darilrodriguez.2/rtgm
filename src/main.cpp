#include<SFML/Graphics.hpp>

int main() {
	sf::RenderWindow window(sf::VideoMode(200, 200), "sfml works");
	sf::CircleShape shape(100.f);
	
	shape.setFillColor(sf::Color::Green);

	while (window.isOpen()) {
		sf::Event event;
		if (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(shape);
		window.display();
	}
	
	return 0;
}


